user='gustavo.fernandes';
pass='G01r98f5';
domain='catg.local';


sudo apt-get install byobu
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak


### set connections to HDs
sudo mkdir /Data
sudo mount /dev/sdb2 /Data

sudo mkdir /Data2
sudo mount /dev/sdc /Data2

sudo chown gustavo.fernandes::gustavo.fernandes Data*

### set connection to other server
sudo mkdir /mnt/storage
sudo mkdir /mnt/public
sudo mkdir /mnt/QNAP
sudo mkdir /mnt/laulab
sudo mkdir /mnt/laulab_DI

sudo mount -t cifs -o username="$user",password="$pass",domain="$domain",uid=$(id -u),gid=$(id -g),forceuid,forcegid, //192.168.1.5/arquivos/Medicina_Personalizada /mnt/QNAP
sudo mount -t cifs -o username="$user",password="$pass",domain="$domain",uid=$(id -u),gid=$(id -g),forceuid,forcegid, //192.168.0.222/arquivos/ /mnt/storage
sudo mount -t cifs -o username="$user",password="$pass",domain="$domain",uid=$(id -u),gid=$(id -g),forceuid,forcegid, //192.168.0.37/public/ /mnt/public
sudo mount -t cifs -o username="$user",password="$pass",domain="$domain",uid=$(id -u),gid=$(id -g),forceuid,forcegid, //192.168.0.37/laulab/ /mnt/laulab
sudo -S mount -t cifs -o vers=1.0,username="$user",password="$pass",domain="$domain",uid=$(id -u),gid=$(id -g),forceuid,forcegid, //192.168.0.37/Setor_Doenças_Infecciosas /mnt/laulab_DI

### set soft links 
mkdir Scripts servidores tools

ln -s /mnt/laulab_DI ~/servidores/Setor_Doenças_Infecciosas
ln -s /mnt/QNAP ~/servidores/QNAP
ln -s /mnt/storage ~/servidores/storage
ln -s /mnt/public ~/servidores/public
ln -s /mnt/laulab ~/servidores/laulab

#Installing softwares
### msgen (microsoft azure CLI access) and  azure-storage-blob (version 0.36.0 needed)
sudo apt-get install -y build-essential libssl-dev libffi-dev libpython-dev python-dev python-pip python3-pip
sudo pip install --upgrade --no-deps msgen
sudo pip install msgen
sudo pip install azure-storage-blob==0.36.0
sudo pip install scipy
sudo pip3 install numpy pandas
### git
sudo apt install git

### R 
#repository for R 3.6.3, for details check https://vps.fmvz.usp.br/CRAN/
echo "deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/" | sudo tee -a /etc/apt/sources.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9

sudo apt-get update
sudo apt-get install r-base r-base-dev

### java jre 
sudo apt-get install default-jre
sudo apt-get install default-jdk
sudo R CMD javareconf

### R libraries
mkdir -p ~/R/x86_64-pc-linux-gnu-library/3.6
Rscript -e 'install.packages("BiocManager",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("ggplot2",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("stringr",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("parallel",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("data.table",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("XLConnect",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("reticulate",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("RJSONIO",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
sudo apt-get install curl libxml2-dev libcurl4-openssl-dev
Rscript -e 'install.packages("fastqcr",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
sudo apt-get install libmariadbclient-dev libpq-dev
Rscript -e 'install.packages("RPostgreSQL",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("dplyr",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("rmarkdown",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("beepr",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("reshape2",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("grid",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("gridExtra",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("devtools",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("shiny",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("shinycssloaders",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("ggpubr",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("shinyFiles",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("shinyWidgets",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("DT",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("plotly",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("fs",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'devtools::install_github("slowkow/ggrepel")'
Rscript -e 'BiocManager::install("SRAdb")'
Rscript -e 'install.packages("emayili",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("magrittr",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("RColorBrewer",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
sudo apt-get install libmagick++-dev
Rscript -e 'install.packages("kableExtra",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("plyr",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'install.packages("xlsx",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'
Rscript -e 'BiocManager::install("ShortRead")'
Rscript -e 'devtools::install_github("vqv/ggbiplot")'
Rscript -e 'install.packages("reticulate",lib="~/R/x86_64-pc-linux-gnu-library/3.6",dependencies=TRUE)'

#docker 
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
docker run hello-world

### cloning git repositories
#samtools/htslib/bcftools/tabix/bgzip
cd tools
wget https://github.com/samtools/samtools/releases/download/1.10/samtools-1.10.tar.bz2
wget https://github.com/samtools/bcftools/releases/download/1.10.2/bcftools-1.10.2.tar.bz2
wget https://github.com/samtools/htslib/releases/download/1.10.2/htslib-1.10.2.tar.bz2

tar -jxvf htslib-1.10.2.tar.bz2
cd htslib-1.10.2
./configure && make && sudo make install 
cd ..

tar -jxvf samtools-1.10.tar.bz2
cd samtools-1.10
./configure && make && sudo make install
cd ..

tar -jxvf bcftools-1.10.2.tar.bz2
cd bcftools-1.10.2
./configure && make && sudo make install
cd ..

#picard
git clone https://github.com/broadinstitute/picard.git
cd picard/
./gradlew shadowJar
cp build/libs/picard.jar ~/tools/picard.jar
cd ..

# basemount
#remember to copy .cfg from/to .basespace
mkdir ~/basespace
mkdir ~/.basespace
sudo bash -c "$(curl -L https://basemount.basespace.illumina.com/install)"
#remember to copy .cfg from/to .basespace

#freebayes
sudo apt install cmake
cd ~/tools
git clone --recursive https://github.com/ekg/freebayes.git
cd freebayes; make

#bedtools
cd ~/tools
wget https://github.com/arq5x/bedtools2/releases/download/v2.29.1/bedtools-2.29.1.tar.gz
tar -zxvf bedtools-2.29.1.tar.gz
cd bedtools2; make

#bwa
cd ~/tools
git clone https://github.com/lh3/bwa.git
cd bwa; make

#vcftools
cd ~/tools
git clone https://github.com/vcftools/vcftools.git
cd vcftools
./autogen.sh
./configure
make
sudo make install

#fastqc
cd ~/tools
wget https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip
unzip fastqc_v0.11.9.zip

#cutadapt
python3 -m pip install --user --upgrade cutadapt

#GATK 3.7

#GATK 4.0
cd ~/tools
wget https://github.com/broadinstitute/gatk/releases/download/4.0.0.0/gatk-4.0.0.0.zip

#GATK 4.1.4.1
cd ~/tools
wget https://github.com/broadinstitute/gatk/releases/download/4.1.4.1/gatk-4.1.4.1.zip

### CdG Custom pipes
#CdG Automated pipeline
cd ~/Scripts
git clone https://gustribeiro@bitbucket.org/gustribeiro/cdg_automation.git /home/gustavo.fernandes/Scripts/CdG_automation

#Nutri
cd ~/Scripts
git clone https://gustribeiro@bitbucket.org/gustribeiro/rsync_ion.git ~/Scripts/snps_4p_nutri

#HCV_PGM
cd ~/Scripts
git clone https://bitbucket.org/GabrielSGoncalves/hcv_pgm/src/master/ ~/Scripts
conda env create --file=~/Scripts/GabrielSGoncalves/hcv_pgm/hcv_environment.yaml

#RSYNC_ION
cd ~/Scripts
git clone https://gustribeiro@bitbucket.org/gustribeiro/rsync_ion.git 

#pcrduplicates
cd ~/tools
git clone  https://github.com/vibansal/PCRduplicates.git
cd PCRduplicates/
make all
